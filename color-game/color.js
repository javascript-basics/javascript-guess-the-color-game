function getRandomColor() {
    const min = 0;
    const max = 255;

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export class Color {
    #red;
    #blue;
    #green;

    constructor() {
        this.#red = getRandomColor();
        this.#blue = getRandomColor();
        this.#green = getRandomColor();
    }

    toString() {
        return `rgb(${this.#red}, ${this.#blue}, ${this.#green})`
    }
}
