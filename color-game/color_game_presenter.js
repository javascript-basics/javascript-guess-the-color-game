import {ColorGameView} from "./color_game_view.js";
import {ColorGameService} from "./color_game_service.js";

const DEFAULT_STATUS = 'Click on Color Element';
const WRONG_STRING = 'Wrong Value. Try again!';
const WINNING_STRING = 'You win!!!';
const WINNING_ANIMATION_TIMEOUT = 2500;

const RETRY_STRING = `${WINNING_STRING} Try again?`;

export class ColorGamePresenter {
    #colorGameView = new ColorGameView();
    #colorGameService = new ColorGameService();

    constructor() {
        this.#init();
    }

    #init() {
        this.#initListeners();
        this.#initBoard();
    }

    #initListeners() {
        (this.#colorGameView.rowControl || new HTMLElement()).addEventListener('change', this.#rowControlHandler.bind(this));
        (this.#colorGameView.columnControl || new HTMLElement()).addEventListener('change', this.#columnControlHandler.bind(this));
        (this.#colorGameView.retryButton || new HTMLElement()).addEventListener('click', this.#initBoard.bind(this));
    }

    #rowControlHandler(control) {
        this.#colorGameService.rows = +(control.target || new HTMLInputElement()).value;
        this.#initBoard();
    }

    #columnControlHandler(control) {
        this.#colorGameService.columns = +(control.target || new HTMLInputElement()).value;
        this.#initBoard();
    }

    #initBoard() {
        this.#colorGameView.updateTitleSectionBackground(null);
        this.#colorGameView.updateStatusLabelContent(DEFAULT_STATUS);

        this.#colorGameService.generateColors();
        this.#colorGameService.initHiddenColor();

        const rows = this.#colorGameService.rows;
        const columns = this.#colorGameService.columns;
        const colors = this.#colorGameService.colors;

        this.#colorGameView.resetColorsSectionContent(null);

        for (let row = 0; row < rows; row++) {
            const rowElement = this.#colorGameView.createRow() || new HTMLElement();

            for (let column = 0; column < columns; column++) {
                const color = colors[row * columns + column];
                const id = `${row}-${column}`

                rowElement.appendChild(this.#colorGameView.createColumn(color, id));
            }

            this.#colorGameView.appendToColorsSection(rowElement);
        }

        this.#updateColor();
        this.#initColumnListeners();
    }

    #updateColor() {
        this.#colorGameView.updateColorContent(this.#colorGameService.hiddenColor);
    }

    #initColumnListeners() {
        const columns = this.#colorGameView.columns;

        for (let column of columns) {
            (column || new HTMLElement()).addEventListener('click', this.#columnHandler.bind(this));
        }
    }

    #columnHandler(clickEvent) {
        const column = (clickEvent || new Event('click')).target || new HTMLElement();
        const selectedColor = column.style.background;

        if (selectedColor !== this.#colorGameService.hiddenColor) {
            this.#colorGameView.addColumnPassedClass(column);
            this.#colorGameView.updateStatusLabelContent(WRONG_STRING);
            return;
        }

        this.#setWinningState();
    }

    #setWinningState() {
        this.#removeColumnListeners();
        this.#colorGameView.updateStatusLabelContent(WINNING_STRING);

        const columnElements = this.#colorGameView.columns || [];
        const columnTimeout = WINNING_ANIMATION_TIMEOUT / columnElements.length;

        for (let i = 0; i < columnElements.length; i++) {
            setTimeout(this.#columnAnimationHandler.bind(this, columnElements[i], this.#colorGameService.hiddenColor), columnTimeout * (i + 1));
        }

        setTimeout(this.#winningStateTimeout.bind(this, this.#colorGameService.hiddenColor), WINNING_ANIMATION_TIMEOUT);
    }

    #removeColumnListeners() {
        const columns = this.#colorGameView.columns;

        for (let column of columns) {
            const clone = (column || new HTMLElement()).cloneNode(true);
            (column || new HTMLElement()).parentNode.replaceChild(clone, column);
        }
    }

    #columnAnimationHandler(column, color) {
        this.#colorGameView.addColumnPassedClass(column);
        this.#colorGameView.updateColumnColor(column, color);
    }

    #winningStateTimeout(color) {
        this.#colorGameView.updateTitleSectionBackground(color);
        this.#colorGameView.updateStatusLabelContent(RETRY_STRING);
    }
}
