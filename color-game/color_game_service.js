import {Color} from "./color.js";

const ROWS_DEFAULT_VALUE = 3;
const COLUMNS_DEFAULT_VALUE = 3;

export class ColorGameService {
    #rows = ROWS_DEFAULT_VALUE;
    #columns = COLUMNS_DEFAULT_VALUE;
    #colors = [];
    #hiddenColor;

    get rows() {
        return this.#rows;
    }

    set rows(value) {
        this.#rows = value;
    }

    get columns() {
        return this.#columns;
    }

    set columns(value) {
        this.#columns = value;
    }

    get colors() {
        return this.#colors;
    }

    get hiddenColor() {
        return this.#hiddenColor;
    }

    generateColors() {
        let colors = [];
        const size = this.#rows * this.#columns;
        while (colors.length < size) {
            colors.push(new Color().toString());

            if (colors.length === size) {
                colors = [...new Set(colors)];
            }
        }

        this.#colors = colors;
    }

    initHiddenColor() {
        if (!this.#colors || !this.#colors.length) {
            return;
        }

        const index = this.#getRandomColorIndex();
        this.#hiddenColor = (this.#colors[index] || new Color()).toString();
    }

    #getRandomColorIndex() {
        const min = 0;
        const max = this.#rows * this.#columns - 1;
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
