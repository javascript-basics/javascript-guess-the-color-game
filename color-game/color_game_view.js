function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

function* elementGenerator(className) {
    const buttons = document.getElementsByClassName(className);
    for (let i = 0; i < buttons.length; i++) {
        yield buttons.item(i);
    }
}

const COLOR_CLASS = 'color-value';
const ROW_CLASS = 'colors-row';
const COLUMN_CLASS = 'colors-column';
const COLORS_SECTION_CLASS = 'colors-section';

const PASSED_VALUE_CLASS = 'passed-value';
const TITLE_CLASS = 'title-section';
const STATUS_CLASS = 'status-label';
const RETRY_BTN_CLASS = 'retry-btn';

const ROW_ID = 'row';
const COLUMN_ID = 'column';
const INPUT_CLASS = 'control-input';

const TAB_KEY = 'Tab';

export class ColorGameView {
    #statusElement = Object.create(HTMLElement.prototype, {});
    #titleSection = Object.create(HTMLElement.prototype, {});
    #colorElement = Object.create(HTMLElement.prototype, {});
    #colorsSection = Object.create(HTMLElement.prototype, {});

    get retryButton() {
        return getElementByClassName(RETRY_BTN_CLASS);
    }

    get rowControl() {
        return document.getElementById(ROW_ID);
    }

    get columnControl() {
        return document.getElementById(COLUMN_ID);
    }

    get columns() {
        return [...(document.getElementsByClassName(COLUMN_CLASS) || document.createDocumentFragment().children)];
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#statusElement = getElementByClassName(STATUS_CLASS);
        this.#titleSection = getElementByClassName(TITLE_CLASS);

        this.#colorElement = getElementByClassName(COLOR_CLASS);
        this.#colorsSection = getElementByClassName(COLORS_SECTION_CLASS);

        this.#disableControlKeys();
    }

    #disableControlKeys() {
        const controls = elementGenerator(INPUT_CLASS);
        for (let control of controls) {
            (control || new HTMLElement()).addEventListener('keydown', event => {
                if (event.key !== TAB_KEY) {
                    event.preventDefault();
                }
            })
        }
    }

    addColumnPassedClass(column) {
        (column || new HTMLElement()).classList.add(PASSED_VALUE_CLASS);
    }

    updateColumnColor(column, color) {
        (column || new HTMLElement()).style.background = color;
    }

    updateTitleSectionBackground(newColor) {
        this.#titleSection.style.background = newColor;
    }

    updateStatusLabelContent(content) {
        this.#statusElement.innerText = content;
    }

    updateColorContent(content) {
        this.#colorElement.innerText = content;
    }

    resetColorsSectionContent() {
        this.#colorsSection.innerHTML = null;
    }

    appendToColorsSection(element) {
        this.#colorsSection.appendChild(element);
    }

    createRow() {
        const rowElement = document.createElement('div');
        rowElement.classList.add(ROW_CLASS);
        return rowElement;
    }

    createColumn(color, id) {
        const columnElement = document.createElement('div');
        columnElement.classList.add(COLUMN_CLASS);
        columnElement.id = id;
        columnElement.style.background = color;
        return columnElement;
    }
}
