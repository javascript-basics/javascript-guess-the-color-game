import {ColorGamePresenter} from "./color-game/color_game_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new ColorGamePresenter();
        }
    }
}

Starter.start();
